<?php

namespace App\Repository;

use App\Entity\UsuariOferta;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UsuariOferta|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsuariOferta|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsuariOferta[]    findAll()
 * @method UsuariOferta[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsuariOfertaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UsuariOferta::class);
    }

    // /**
    //  * @return UsuariOferta[] Returns an array of UsuariOferta objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsuariOferta
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
