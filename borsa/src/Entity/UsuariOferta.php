<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use App\Entity\Usuari;
use App\Entity\Ofertes;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsuariOfertaRepository")
 */
class UsuariOferta
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @ManyToOne(targetEntity="App\Entity\Usuari", inversedBy="correu")
     */
    private $correu_user;

    /**
     * @ORM\Column(type="integer")
     * @ManyToOne(targetEntity="App\Entity\Ofertes", inversedBy="id")
     */
    private $ofertaid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $hora;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCorreuUser(): ?string
    {
        return $this->correu_user;
    }

    public function setCorreuUser(string $correu_user): self
    {
        $this->correu_user = $correu_user;

        return $this;
    }

    public function getOfertaid(): ?int
    {
        return $this->ofertaid;
    }

    public function setIdOferta(int $ofertaid): self
    {
        $this->ofertaid = $ofertaid;

        return $this;
    }

    public function getHora(): ?\DateTimeInterface
    {
        return $this->hora;
    }

    public function setHora(\DateTimeInterface $hora): self
    {
        $this->hora = $hora;

        return $this;
    }
}
