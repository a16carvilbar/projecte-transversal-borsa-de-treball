<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use App\Entity\UsuariOferta;
/**
 * @ORM\Entity(repositoryClass="App\Repository\UsuariRepository")
 */
class Usuari
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @OneToMany(targetEntity="App\Entity\UsuariOferta", mappedBy="correu_user")
     */
    private $correu;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $cog1;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $cog2;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCorreu(): ?string
    {
        return $this->correu;
    }

    public function setCorreu(string $correu): self
    {
        $this->correu = $correu;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getCog1(): ?string
    {
        return $this->cog1;
    }

    public function setCog1(string $cog1): self
    {
        $this->cog1 = $cog1;

        return $this;
    }

    public function getCog2(): ?string
    {
        return $this->cog2;
    }

    public function setCog2(?string $cog2): self
    {
        $this->cog2 = $cog2;

        return $this;
    }
}
