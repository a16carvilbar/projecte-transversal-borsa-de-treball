<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use App\Entity\UsuariOferta;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OfertesRepository")
 */
class Ofertes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @OneToMany(targetEntity="App\Entity\UsuariOferta", mappedBy="ofertaid")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $titol;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $descripcio;

    /**
     * @ORM\Column(type="datetime")
     */
    private $data_pub;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $ubicacio;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria", inversedBy="categoria")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categoria;

    /**
     * @ORM\Column(type="boolean")
     */
    
    private $aceptada = false;
    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $nomEmpresa;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $webEmpresa;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $email_empresa;
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitol(): ?string
    {
        return $this->titol;
    }

    public function setTitol(string $titol): self
    {
        $this->titol = $titol;

        return $this;
    }

    public function getDescripcio(): ?string
    {
        return $this->descripcio;
    }

    public function setDescripcio(string $descripcio): self
    {
        $this->descripcio = $descripcio;

        return $this;
    }

    public function getDataPub(): ?\DateTimeInterface
    {
        return $this->data_pub;
    }

    public function setDataPub(\DateTimeInterface $data_pub): self
    {
        $this->data_pub = $data_pub;

        return $this;
    }

    public function getUbicacio(): ?string
    {
        return $this->ubicacio;
    }

    public function setUbicacio(string $ubicacio): self
    {
        $this->ubicacio = $ubicacio;

        return $this;
    }

    public function getCategoria(): ?categoria
    {
        return $this->categoria;
    }

    public function setCategoria(categoria $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }

    public function getAceptada(): ?bool
    {
        return $this->aceptada;
    }

    public function setAceptada(bool $aceptada): self
    {
        $this->aceptada = $aceptada;

        return $this;
    }

    public function getNomEmpresa(): ?string
    {
        return $this->nomEmpresa;
    }

    public function setNomEmpresa(string $nomEmpresa): self
    {
        $this->nomEmpresa = $nomEmpresa;

        return $this;
    }

    public function getWebEmpresa(): ?string
    {
        return $this->webEmpresa;
    }

    public function setWebEmpresa(string $webEmpresa): self
    {
        $this->webEmpresa = $webEmpresa;

        return $this;
    }

    public function getEmailEmpresa(): ?string
    {
        return $this->email_empresa;
    }

    public function setEmailEmpresa(string $email_empresa): self
    {
        $this->email_empresa = $email_empresa;

        return $this;
    }


}
