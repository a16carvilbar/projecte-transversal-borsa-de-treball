<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190320073946 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ofertes DROP FOREIGN KEY FK_2CA5E0F710560508');
        $this->addSql('DROP INDEX UNIQ_2CA5E0F710560508 ON ofertes');
        $this->addSql('ALTER TABLE ofertes CHANGE id_categoria_id categoria_id INT NOT NULL');
        $this->addSql('ALTER TABLE ofertes ADD CONSTRAINT FK_2CA5E0F73397707A FOREIGN KEY (categoria_id) REFERENCES categoria (id)');
        $this->addSql('CREATE INDEX IDX_2CA5E0F73397707A ON ofertes (categoria_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ofertes DROP FOREIGN KEY FK_2CA5E0F73397707A');
        $this->addSql('DROP INDEX IDX_2CA5E0F73397707A ON ofertes');
        $this->addSql('ALTER TABLE ofertes CHANGE categoria_id id_categoria_id INT NOT NULL');
        $this->addSql('ALTER TABLE ofertes ADD CONSTRAINT FK_2CA5E0F710560508 FOREIGN KEY (id_categoria_id) REFERENCES categoria (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2CA5E0F710560508 ON ofertes (id_categoria_id)');
    }
}
