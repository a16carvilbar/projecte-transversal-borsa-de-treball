<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190326100232 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE usuari_oferta CHANGE correu correu_user VARCHAR(255) NOT NULL, CHANGE id_oferta ofertaid INT NOT NULL');
        $this->addSql('ALTER TABLE ofertes ADD email_empresa VARCHAR(80) NOT NULL, CHANGE nom_empresa nom_empresa VARCHAR(80) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ofertes DROP email_empresa, CHANGE nom_empresa nom_empresa VARCHAR(80) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE usuari_oferta CHANGE correu_user correu VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE ofertaid id_oferta INT NOT NULL');
    }
}
