<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190318111759 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ofertes CHANGE id_categoria id_categoria_id INT NOT NULL');
        $this->addSql('ALTER TABLE ofertes ADD CONSTRAINT FK_2CA5E0F710560508 FOREIGN KEY (id_categoria_id) REFERENCES categoria (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2CA5E0F710560508 ON ofertes (id_categoria_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ofertes DROP FOREIGN KEY FK_2CA5E0F710560508');
        $this->addSql('DROP INDEX UNIQ_2CA5E0F710560508 ON ofertes');
        $this->addSql('ALTER TABLE ofertes CHANGE id_categoria_id id_categoria INT NOT NULL');
    }
}
