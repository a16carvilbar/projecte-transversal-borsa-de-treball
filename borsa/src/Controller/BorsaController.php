<?php
// src/Controller/BorsaController.php
namespace App\Controller;

// ...

use App\Entity\Ofertes;
use App\Entity\Categoria;
use App\Entity\Logs;
use App\Entity\Usuari;
use App\Entity\OfertesPendents;
use App\Entity\UsuariOferta;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\JsonResponse;

class BorsaController extends AbstractController{

    ////////////////////////////////////
    //                                //
    //             ADMIN              //
    //                                //
    //////////////////////////////////// 

    /**
    * @Route("/", name="index")
    */
    public function retorn_index(){
        $oferta = $this->getDoctrine()->getRepository(Ofertes::class)->findAll();
        $mida_oferta_totals = sizeof($oferta);
        
        $mida_ofertes_publicades = 0;
        $ofertes_ASIX = 0;
        $ofertes_DAW = 0;
        $ofertes_DAM = 0;
        $ofertes_SMIX = 0;

        for ($i=0; $i< sizeof($oferta); $i++){

            $categoria = $oferta[$i]-> getCategoria() -> getNom();
            
            if ($categoria == "ASIX"){
                $ofertes_ASIX = $ofertes_ASIX +1;
            }else if ($categoria == "DAW"){
                $ofertes_DAW = $ofertes_DAW +1;
            }else if ($categoria == "DAM"){
                $ofertes_DAM = $ofertes_DAM +1;
            }else if ($categoria == "SMIX"){
                $ofertes_SMIX = $ofertes_SMIX +1;
            }

            $acceptat = $oferta[$i]->getAceptada();
            if($acceptat == "1"){
                $mida_ofertes_publicades = $mida_ofertes_publicades + 1;
            }
        }

        $mida_ofertes_pendents = $mida_oferta_totals - $mida_ofertes_publicades;

        $users = $this->getDoctrine()->getRepository(Usuari::class)->findAll();
        $mida_users = sizeof($users);

        return $this->render('index.html.twig',
        [
        'users' => $mida_users,
        'ofertes_pendents' => $mida_ofertes_pendents,
        'ofertes_publicades' => $mida_ofertes_publicades,
        'ofertes_totals' => $mida_oferta_totals,
        'ofertes_ASIX' => $ofertes_ASIX,
        'ofertes_DAW' => $ofertes_DAW,
        'ofertes_DAM' => $ofertes_DAM,
        'ofertes_SMIX' => $ofertes_SMIX
        ]);
    }


    ////////////////////////////////////
    //                                //
    //            OFERTES             //
    //                                //
    //////////////////////////////////// 

    /**
    * @Route("/ofertesPendents", name="ofertesPendents")
    */
    public function mostra_ofertesPendents(){
        
        $oferta = $this->getDoctrine()->getRepository(Ofertes::class)->findAll();
        $inscripcio = $this->getDoctrine()->getRepository(UsuariOferta::class)->findAll();
        $array=[];
        $array2=[];
        for ($i=0; $i< sizeof($oferta); $i++){
            
            $id= $oferta[$i]->getId();
            $titol= $oferta[$i]->getTitol();
            $nomEmpresa = $oferta[$i]->getNomEmpresa();
            $descripcio= $oferta[$i]->getDescripcio();
            $data_pub= $oferta[$i]-> getDataPub() -> format('Y-m-d H:i:s');
            $categoria= $oferta[$i]-> getCategoria();
            $ubicacio= $oferta[$i]-> getUbicacio();
            $webEmpresa = $oferta[$i]->getWebEmpresa();
            $acceptat = $oferta[$i]->getAceptada();
            $email_empresa = $oferta[$i]->getEmailEmpresa();

            $array2[$i] = 0;

            for ($j=0; $j< sizeof($inscripcio); $j++){
                    
                $ofertaid= $inscripcio[$j]->getOfertaid();
                if ($ofertaid == $id) {
                        $array2[$i]= $array2[$i]+1;
                }
            }

            $inscripcions=$array2[$i];
            array_push($array,[$id,$nomEmpresa,$titol,$descripcio,$data_pub,$categoria,$ubicacio,$webEmpresa,$acceptat,$email_empresa,$inscripcions]);
        }
        
        return $this->render('ofertesPendents.html.twig' ,['ofertes' => $array]);
    }
    /**
    * @Route("/ofertes", name="ofertes")
    */

    public function ofertes_categoria(Request $request){

        $form = $this->createFormBuilder()
            ->add('categoria', EntityType::class, [
                 // looks for choices from this entity
                 'class' => Categoria::class,
            
                 // uses the User.username property as the visible option string
                 'choice_label' => 'nom',
                 'attr' => ['class' => 'form-control']
            ])     

            ->add('save', SubmitType::class, ['label' => 'Buscar '])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form-> isValid()){
        
            $categoria = $form->getData();

            $oferta = $this->getDoctrine()->getRepository(Ofertes::class)->findBy($categoria);
            $inscripcio = $this->getDoctrine()->getRepository(UsuariOferta::class)->findAll();
            $array=[];
            $array2=[];
            for ($i=0; $i< sizeof($oferta); $i++){
            
                $id= $oferta[$i]->getId();
                $titol= $oferta[$i]->getTitol();
                $nomEmpresa = $oferta[$i]->getNomEmpresa();
                $descripcio= $oferta[$i]->getDescripcio();
                $data_pub= $oferta[$i]-> getDataPub() -> format('Y-m-d H:i:s');
                $categoria= $oferta[$i]-> getCategoria();
                $ubicacio= $oferta[$i]-> getUbicacio();
                $webEmpresa = $oferta[$i]->getWebEmpresa();
                $acceptat = $oferta[$i]->getAceptada();
                $email_empresa = $oferta[$i]->getEmailEmpresa();

                $array2[$i] = 0;

                for ($j=0; $j< sizeof($inscripcio); $j++){
                    
                    $ofertaid= $inscripcio[$j]->getOfertaid();
                    if ($ofertaid == $id) {
                        $array2[$i]= $array2[$i]+1;
                    }
                }

                $inscripcions=$array2[$i];
            array_push($array,[$id,$nomEmpresa,$titol,$descripcio,$data_pub,$categoria,$ubicacio,$webEmpresa,$acceptat,$email_empresa,$inscripcions]);
        }
        
        return $this->render('ofertes.html.twig' ,['ofertes' => $array]);
        
    }
    return $this->render('form_ofertesPublicades.html.twig' ,['form' => $form->createview(),]);
}

    /**
    * @Route("/afegir_oferta", name="afegir_oferta")
    */

    public function form(Request $request){

        $oferta = new Ofertes();
        $categoria = new Categoria();
        $form = $this->createFormBuilder($oferta)
            ->add('titol', TextType::class, [
                'label' => 'Títol',
                'attr' => ['class' => 'form-control'],
            ])
            ->add('nomEmpresa', TextType::class, [
                'label' => 'Nom empresa',
                'attr' => ['class' => 'form-control'],
            ])
            ->add('descripcio', TextareaType::class, [
                'label' => 'Descripció',
                'attr' => ['class' => 'form-control'],
            ])
 
            ->add('categoria', EntityType::class, [
                 // looks for choices from this entity
                 'class' => Categoria::class,
            
                 // uses the User.username property as the visible option string
                 'choice_label' => 'nom',
                 'attr' => ['class' => 'form-control']

            ])
            ->add('ubicacio', TextType::class, [
                'label' => 'Ubicació',
                'attr' => ['class' => 'form-control'],
            ])

            ->add('webEmpresa', TextType::class, [
                'label' => 'Pàgina Web',
                'data' => 'http://',
                'attr' => ['class' => 'form-control'],
            ])
            ->add('emailEmpresa', TextType::class, [
                'label' => 'Correu',
                'attr' => ['class' => 'form-control'],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Crear',
                'attr' => ['class' => 'btn btn-primary'],
            ])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form-> isValid()){
            $oferta->setDataPub(new \DateTime("now"));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($oferta);
            $entityManager->flush();
            return $this->redirect('http://192.168.205.166');
        }
        return $this->render('inserir_oferta.html.twig' ,['form' => $form->createview(),]);
    }

    /**
    * @Route("/editar_oferta/{id}", name="editar_oferta")
    */
    public function form_Buscar_Editar($id,Request $request){

        $oferta = new Ofertes();
        
        $oferta = $this->getDoctrine()->getRepository(Ofertes::class)->find($id);

        $titol= $oferta->getTitol();
        $nomEmpresa = $oferta->getNomEmpresa();
        $descripcio= $oferta->getDescripcio();
        $data_pub= $oferta-> getDataPub() -> format('Y-m-d H:i:s');
        $categoria= $oferta-> getCategoria();
        $ubicacio= $oferta-> getUbicacio();
        $webEmpresa = $oferta->getWebEmpresa();
            
        $form = $this->createFormBuilder($oferta)
            ->add('titol', TextType::class, array(
                'label'=> 'Titol ',
                'attr' => ['class' => 'form-control'],
                'data' => $titol
            ))
            ->add('nomEmpresa', TextType::class, array(
                'label'=> 'Nom Empresa ',
                'attr' => ['class' => 'form-control'],
                'data' => $nomEmpresa
            ))
            ->add('descripcio', TextareaType::class, array(
                'label'=> 'Descripció',
                'attr' => ['class' => 'form-control'],
                'data' => $descripcio
            ))
            ->add('categoria', EntityType::class, [
                'class' => Categoria::class,
                'attr' => ['class' => 'form-control'],
                'choice_label' => 'nom',
           ])
            ->add('ubicacio', TextType::class, array(
                'label'=> 'Ubicació ',
                'attr' => ['class' => 'form-control'],
                'data' => $ubicacio
            ))
            ->add('webEmpresa', TextType::class, array(
                'label'=> 'Web Empresa ',
                'attr' => ['class' => 'form-control'],
                'data' => $webEmpresa
            ))
            ->add('save', SubmitType::class, [
                'label' => 'Guardar ',
                'attr' => ['class' => 'btn btn-primary']
            ])
            ->getForm();

            $form->handleRequest($request);

            if($form->isSubmitted() && $form-> isValid()){
                
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->flush();
                if ($oferta->getAceptada())
                return $this->redirectToRoute('ofertes');
                return $this->redirectToRoute('ofertesPendents');
            }

            return $this->render('editar_oferta.html.twig' ,['form' => $form->createview(),]);
    }

    
    /**
    * @Route("/aceptar_oferta/{id}", name="aceptar_oferta")
    */
    
    public function aceptar_oferta($id){
        $em = $this->getDoctrine()->getEntityManager();
        $oferta = $em->getRepository(Ofertes::class)->find($id);

        if (!$oferta){
            throw $this->createNotFoundException(
                'No intentis editar per aqui el que no existeix D:'
            );
        }

        $oferta->setAceptada(true);
        $em->flush();   

        return $this->redirectToRoute('ofertesPendents');
}

    /**
    * @Route("/despublicar_oferta/{id}", name="despublicar_oferta")
    */
    
    public function despublicar_oferta($id){
        $em = $this->getDoctrine()->getEntityManager();
        $oferta = $em->getRepository(Ofertes::class)->find($id);
    
        if (!$oferta){
            throw $this->createNotFoundException(
                'No intentis editar per aqui el que no existeix D:'
            );
        }
    
        $oferta->setAceptada(false);
        $em->flush();   
    
        return $this->redirectToRoute('ofertes');
    }
    
    /**
    * @Route("/esborrar_oferta/{id}", name="esborrar_oferta")
    */

    public function esborrar_oferta($id)
{
        $em = $this->getDoctrine()->getEntityManager();
        $oferta = $em->getRepository(Ofertes::class)->find($id);

        if (!$oferta){
            throw $this->createNotFoundException(
                'No intentis editar per aqui el que no existeix D:'
            );
        }

        $em->remove($oferta);
        $em->flush();   
        if ($oferta->getAceptada())
        return $this->redirectToRoute('ofertes');
        return $this->redirectToRoute('ofertesPendents');
}

    ////////////////////////////////////
    //                                //
    //            USUARIS             //
    //                                //
    //////////////////////////////////// 

    /**
    * @Route("/usuaris")
    */
    public function mostra_usuaris(){
        
        $user = $this->getDoctrine()->getRepository(Usuari::class)->findAll();
        $array=[];
        for ($i=0; $i< sizeof($user); $i++){
            
            $id= $user[$i]->getId();
            $correu= $user[$i]->getCorreu();
            $password= $user[$i]->getPassword();
            $nom= $user[$i]->getNom();
            $cog1= $user[$i]->getCog1();
            $cog2= $user[$i]->getCog2();
            
            array_push($array,[$id,$correu,$password,$nom,$cog1,$cog2]);
            
        }
        
        return $this->render('usuaris.html.twig' ,['usuari' => $array]);
    }


    ////////////////////////////////////
    //                                //
    //              API               //
    //                                //
    //////////////////////////////////// 

    /**
    * @Route("/api", name="ofertes_client")
    */
    
    public function api(){
        
        $oferta = $this->getDoctrine()->getRepository(Ofertes::class)->findBy(array(), array('data_pub' => 'DESC'));
        $array=[];
        for ($i=0; $i< sizeof($oferta); $i++){
            $acceptat = $oferta[$i]->getAceptada();
            if ($acceptat) {
            $id= $oferta[$i]->getId();
            $titol= $oferta[$i]->getTitol();
            $nomEmpresa = $oferta[$i]->getNomEmpresa();
            $descripcio= $oferta[$i]->getDescripcio();
            $data_pub= $oferta[$i]-> getDataPub() -> format('Y-m-d H:i:s');
            $categoria= $oferta[$i]-> getCategoria() -> getNom();
            $ubicacio= $oferta[$i]-> getUbicacio();
            $webEmpresa = $oferta[$i]->getWebEmpresa();
            
            array_push($array,[$id,$nomEmpresa,$titol,$descripcio,$data_pub,$categoria,$ubicacio,$webEmpresa,$acceptat]);
            }
        }
        return new JsonResponse(array('ofertes' => $array));
        
    }

    /**
    * @Route("/api/usuari/{correu}", name="usuaris_api")
    */
    
    public function api_usuaris($correu){
        
        $usuari = $this->getDoctrine()->getRepository(Usuari::class)->findAll();
        
        $registrat = false;
        
        for ($i=0; $i< sizeof($usuari); $i++){
            
            $correu_user= $usuari[$i]->getCorreu();
            
            if($correu_user == $correu){

                $registrat = true;
                
            }
        }

        return new JsonResponse($registrat);

    }

    /**
    * @Route("/api/afegir_usuari/{correu}/{nom}/{cognom}", name="agefir_usuaris_api")
    */
    
    public function api_afegir_usuaris($correu, $nom, $cognom){
        
        $entityManager = $this->getDoctrine()->getManager();
        
        $usuari = new Usuari();
        $usuari->setCorreu($correu);
        $usuari->setNom($nom);
        $usuari->setCog1($cognom);

        $entityManager->persist($usuari);
        $entityManager->flush();

        return new JsonResponse($usuari);
    }

    /**
    * @Route("/api/esta_suscripcio/{correu}/{id_oferta}", name="esta_suscripcio")
    */
    
    public function esta_suscripcio($correu, $id_oferta){
        
        $usuari_oferta = $this->getDoctrine()->getRepository(UsuariOferta::class)->findAll();
        
        $registrat = false;

        for ($i=0; $i< sizeof($usuari_oferta); $i++){
            
            $correu_user_db = $usuari_oferta[$i]->getCorreuUser();
            $id_oferta_db = $usuari_oferta[$i]->getOfertaid();
            
            if(($id_oferta_db == $id_oferta)&&($correu_user_db == $correu)){

                $registrat = true;
                
            }
        }

        return new JsonResponse($registrat);

    }

    /**
    * @Route("/api/usuari_a_oferta/{correu}/{id_oferta}", name="usuari_a_oferta")
    */
    
    public function usuari_a_oferta($correu, $id_oferta){
        
        $entityManager = $this->getDoctrine()->getManager();
        $hora_i_temps_actual = new \DateTime("now");

        $usuarioferta = new UsuariOferta();
        $usuarioferta->setCorreuUser($correu);
        $usuarioferta->setIdOferta($id_oferta);
        
        $usuarioferta->setHora($hora_i_temps_actual);

        $entityManager->persist($usuarioferta);
        $entityManager->flush();
        
        $correu_user = $correu;
        return new JsonResponse($usuarioferta);
    }


    ////////////////////////////////////
    //                                //
    //             MAIL               //
    //                                //
    //////////////////////////////////// 


    /**
    * @Route("/email/{correu_to}", name="email")
    */
    public function index($correu_to, Request $request, \Swift_Mailer $mailer)
    {
    
    //$correue = $request->request->get('correu_empresa');
    $message = (new \Swift_Message('Confirmacio inscripció'))
        ->setFrom('jobiam.cat@gmail.com')
        ->setTo($correu_to) //aqui hi va el correu
        ->setBody(
            $this->renderView(
                // templates/emails/registration.html.twig
                'email.html.twig',
                ['name' => 'oriol']
            ),
            'text/html'
        );

    $mailer->send($message);

    return new JsonResponse($correu_to);

    }
}