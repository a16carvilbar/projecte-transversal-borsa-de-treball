<?php
  require '../ip_maquina.php';
?>

<script>

$(document).ready(function(){

  categoria_oferta_post = '<?php echo $categoria_oferta;?>'

  console.log(categoria_oferta_post);

    let html="";
    let ids_ofertes=[];

    $.ajax({
      method: "GET",
      url: "http://<?=$ip_maquina?>:8000/api",
    })
    .done(function(res) {
      ofertes = res["ofertes"];
      ofertaDAW = 0;
      ofertaDAM = 0;
      ofertaASIX = 0;
      ofertaSMIX = 0;

      for (let c=0; c < ofertes.length; c++){
  
        num_oferta = c;
        id_oferta = ofertes[c][0];
        nom_empresa = ofertes[c][1];
        titol_oferta = ofertes[c][2];
        descripcio = ofertes[c][3];
        data_pub = ofertes[c][4];
        categoria_oferta = ofertes[c][5];
        url_empresa = ofertes[c][7];
        ubicacio_oferta = ofertes[c][6];
        ids_ofertes[c] = id_oferta;
        
        // Agafem la data actual //
        d = new Date();
        any = d.getFullYear();
        
        mes = d.getMonth()+1;
        if (mes < 10){
          mes = "0"+mes;
        }

        dia = d.getDate();
        data_actual = any+"-"+mes+"-"+dia;

        // Agafem la data de la publicacio //
        any_pub = data_pub.substring(4,0);
        mes_pub = data_pub.substring(5,7);
        dia_pub = data_pub.substring(8,10);
        data_pub = any_pub+"-"+mes_pub+"-"+dia_pub;

        // Diferencia de dies amb les 2 dates //
        var oneDay = 24*60*60*1000; 
        var firstDate = new Date(any_pub,mes_pub,dia_pub);
        var secondDate = new Date(any,mes,dia);
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
        
        // colors categoria i contador de les ofertes //
        if(ofertes[c][5]=="DAW"){
          if (diffDays < 90){
            ofertaDAW++;
          }
          color = "success";
        }
        else if(ofertes[c][5]=="DAM"){
          if (diffDays < 90){
            ofertaDAM++;
          }
          color = "warning";
        }
        else if(ofertes[c][5]=="ASIX"){
          if (diffDays < 90){
            ofertaASIX++;
          }
          color = "danger";
        }
        else if(ofertes[c][5]=="SMIX"){
          if (diffDays < 90){
            ofertaSMIX++;
          }
          color = "info";
        }

        // OFERTA //

        // Retorna la oferta que coinsideix amb la oferta // 
        if(categoria_oferta_post == ofertes[c][5]){
          // Si la publicacio te mes de 3 mesos no es publica //
          if (diffDays < 90){
            // Si la publicacio te mes de 15 dies s'aplica una class //
            if (diffDays <= 15){
              html+='<div id= oferta_'+num_oferta+' class="oferta_box job-post-item bg-white p-4 d-block d-md-flex align-items-center">';
            }else{
              html+='<div id= oferta_'+num_oferta+' class="old_oferta oferta_box job-post-item bg-white p-4 d-block d-md-flex align-items-center">';
            }
                html+=`<div class="mb-4 mb-md-0 mr-5">
                        <div class="job-post-item-header d-flex align-items-center">
                            <h2 class="mr-3 text-black h4">${titol_oferta}</h2>
                            <div class="badge-wrap">
                                <span class="bg-${color} text-white badge py-2 px-4">${categoria_oferta}</span>
                            </div>
                        </div>
                        <div class="job-post-item-body d-block d-md-flex">
                            <div class="mr-3"><span class="fl-bigmug-line-portfolio23"></span><a href="${url_empresa}"> ${ nom_empresa}</a></div>
                            <div><span class="fl-bigmug-line-big104"></span><span> ${ ubicacio_oferta}</span></div>
                        </div>
                        <div id="descripcio_${num_oferta}"></div></div>
                        <div id="boto_susc_${num_oferta}" class="ml-auto"></div>
                      </div>`;
        }

         // Retorna la oferta que coinsideix amb la oferta // 
        }else if(categoria_oferta_post == "totes"){
          // Si la publicacio te mes de 3 mesos no es publica //
          if (diffDays < 90){
            // Si la publicacio te mes de 15 dies s'aplica una class //
            if (diffDays <= 15){
              html+='<div id= oferta_'+num_oferta+' class="oferta_box job-post-item bg-white p-4 d-block d-md-flex align-items-center">';
            }else{
              html+='<div id= oferta_'+num_oferta+' class="old_oferta oferta_box job-post-item bg-white p-4 d-block d-md-flex align-items-center">';
            }
            html+=`<div class="mb-4 mb-md-0 mr-5">
                    <div class="job-post-item-header d-flex align-items-center">
                      <h2 class="mr-3 text-black h4">${titol_oferta}</h2>
                      <div class="badge-wrap">
                        <span class="bg-${color} text-white badge py-2 px-4">${categoria_oferta}</span>
                      </div>
                    </div>
                    <div class="job-post-item-body d-block d-md-flex">
                      <div class="mr-3"><span class="fl-bigmug-line-portfolio23"></span><a href="${url_empresa}"> ${ nom_empresa}</a></div>
                        <div><span class="fl-bigmug-line-big104"></span><span> ${ ubicacio_oferta}</span></div>
                        </div>
                        <div id="descripcio_${num_oferta}"></div>
                      </div>
                      <div id="boto_susc_${num_oferta}" class="ml-auto"></div>
                  </div>`;

          }
        }
     
    }
    document.getElementById("DAW").innerHTML = "DAW (" + ofertaDAW +")";
    document.getElementById("DAM").innerHTML = "DAM (" + ofertaDAM +")";
    document.getElementById("ASIX").innerHTML = "ASIX (" + ofertaASIX +")";
    document.getElementById("SMIX").innerHTML = "SMIX (" + ofertaSMIX +")";

    // Mostra la oferta //
    document.getElementById("oferta").innerHTML = html;
     

    // S'agafa totes les cookies per poder agafar la desitjada //
    var allcookies = document.cookie;
    cookiearray = allcookies.split(';');
    for(var i=0; i<cookiearray.length; i++) {
      name = cookiearray[i].split('=')[0];
      value = cookiearray[i].split('=')[1];
    }

    correu_user = cookiearray[1];

    if (typeof correu_user !== 'undefined'){
      correu_user = correu_user.split('=');
      correu_user = correu_user[1];
      console.log(correu_user);
    }
    
    $.ajax({
      method: "GET",
      url: "http://<?=$ip_maquina?>:8000/api/usuari/"+correu_user+"",
    })
    .done(function(result) {
      console.log("Correu es "+result+", pots veure les descripcions");
      user_registrat = result;

      // SI l'usuari esta registrat podra interactuar amb el boto //
      console.log("Pots interactuar? "+user_registrat);
      if(user_registrat==true){ 

        // Al fer click a la oferta selecionada s'amplia amb la descripcio//
        $( "#oferta" ).on( "click", ".oferta_box", function() {
        
          id_oferta_total = $(this).attr("id");
          id_oferta_num = id_oferta_total.substring(7);

          descripcio = ofertes[id_oferta_num][3];
          
          id_descripcio = "descripcio_"+id_oferta_num;
          id_boto_susc = "boto_susc_"+id_oferta_num;

        
          if ($("#"+id_oferta_total).hasClass("desc_oculta")){
          $("#"+id_oferta_total).removeClass("desc_oculta");
          document.getElementById(""+id_descripcio).innerHTML = "";
          document.getElementById(""+id_boto_susc).innerHTML = "";

          }else{
            //Mostra el boto i la descripcio, si no es troba la classe desc_oculta
            $("#"+id_oferta_total).addClass("desc_oculta");
            document.getElementById(""+id_descripcio).innerHTML = descripcio;
            
            boto_susc ="<button class='boto_suscripcio btn btn-primary py-2' type='submit'> Apuntam </button>";
            document.getElementById(""+id_boto_susc).innerHTML = boto_susc;

          }
        });

        // Al fer click al boto s'envia una peticio a symfoni demanant si ja esta aquesta suscripcio//
        $( "#oferta" ).on( "click", ".boto_suscripcio", function() {

          id_oferta = ids_ofertes[id_oferta_num];

          // Si la suscripcio no apareix es creara // 
          $.ajax({
                  method: "GET",
                  url: "http://<?=$ip_maquina?>:8000/api/esta_suscripcio/"+correu_user+"/"+id_oferta+"",
                })
                .done(function(result) {

                  if (result == false){
                    swal({
                      title: correu_user,
                      text: "Preparant els papers...",
                      icon: "warning",
                      showConfirmButton: false,
                    });
                    
                    $.ajax({
                      method: "GET",
                      url: "http://<?=$ip_maquina?>:8000/api/usuari_a_oferta/"+correu_user+"/"+id_oferta+"",
                    })
                    .done(function(result) {
                        $.ajax({
                        method: "GET",
                        url: "http://<?=$ip_maquina?>:8000/email/"+correu_user+"",
                      })
                      .done(function(result) {

                        swal({
                        title: correu_user,
                        text: "Has sigut inscrit en l'oferta!",
                        icon: "success",
                        });
                      })
                    })
                  }else{
                    swal({
                      title: correu_user,
                      text: "Ja t'has registrat en aquesta oferta!",
                      icon: "error",
                    });
                  }
                
                })
        });
      }
    })
  })
});

</script>