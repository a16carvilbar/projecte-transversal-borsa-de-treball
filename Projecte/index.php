<?php
  require '../ip_maquina.php';  
?>

<!DOCTYPE html>
<html lang="cat">

<head>
  <title>JobIAM.cat &mdash; Borsa de treball</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="google-signin-client_id" content="904114278999-tllfjt3rd4jd81m8ap1fit6uk0smo5pd.apps.googleusercontent.com">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
  <script src="https://apis.google.com/js/api:client.js"></script>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900|Roboto+Mono:300,400,500">
  <link rel="stylesheet" href="fonts/icomoon/style.css">
  <script src="https://apis.google.com/js/platform.js" async defer></script>

  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/magnific-popup.css">
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" href="css/bootstrap-datepicker.css">
  <link rel="stylesheet" href="css/animate.css">


  <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
  <link rel="stylesheet" href="css/fl-bigmug-line.css">

  <link rel="stylesheet" href="css/aos.css">

  <link rel="stylesheet" href="css/style.css">
  <script>
    
    // si l'usuari ha iniciat sessio es true //
    let user_registrat = false;

    var googleUser = {};
    var startApp = function () {
      gapi.load('auth2', function () {
        auth2 = gapi.auth2.init({
          client_id: '904114278999-tllfjt3rd4jd81m8ap1fit6uk0smo5pd.apps.googleusercontent.com',
          cookiepolicy: 'single_host_origin',
        });
        attachSignin(document.getElementById('customBtn'));
      });
    };
    
    function attachSignin(element) {
      auth2.attachClickHandler(element, {},
        function (googleUser) {

          // S'agafa informacio basica de l'usauri de google.
          correu_user = googleUser.w3.U3;
          name_user = googleUser.w3.ofa;
          cognom_user = googleUser.w3.wea;
          
          $.ajax({
            method: "GET",
            url: "http://<?=$ip_maquina?>:8000/api/usuari/"+correu_user+"",
          })
          .done(function(result) {

            console.log("Aquest correu es "+result);
   
            if (result == false ){
              
              $.ajax({
                method: "GET",
                url: "http://<?=$ip_maquina?>:8000/api/afegir_usuari/"+correu_user+"/"+name_user+"/"+cognom_user+"",
              })
              .done(function(result) {
                console.log("Afegit");
                swal({
                      title: correu_user,
                      text: "Has sigut donat d'alta a la nostra paguina Web",
                      icon: "success",
                });
              })

            }
              document.cookie = "user_registrat="+correu_user;

            // Redireccio de l'usuari admin //
            if (correu_user == "<?=$correu_admin?>"){
              window.location.replace("http://<?=$ip_maquina?>:8000");
              console.log("hola admin");
            }else{
              location.reload();
            }
              
          })
        });
    };

    
    function signOut() {
      var auth2 = gapi.auth2.getAuthInstance();
      auth2.signOut().then(function () {
        console.log('User signed out');
        document.cookie = "user_registrat='null'";
        location.reload();

      });
    }
  </script>
</head>

<body>

  <div class="site-wrap">
    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>

    <header class="site-navbar py-1" role="banner">

      <div class="container">
        <div class="row align-items-center">

          <div class="col-6 col-xl-2">
            <h1 class="mb-0"><a href="/" class="text-black h2 mb-0">Job<strong>IAM.cat</strong></a></h1>
          </div>

          <div class="col-10 col-xl-10 d-none d-xl-block">
            <nav class="site-navigation text-right" role="navigation">

              <ul class="site-menu js-clone-nav mr-auto d-none d-lg-block">
                <li id="signOut" class="ml-auto">
                  <span class="btn rounded bg-primary py-2 px-3 text-white" onclick="signOut()">Tanca Sessió</span>
                </li>
                <li id="signIn" class="ml-auto">
                  <span id="customBtn" class="btn rounded bg-primary py-2 px-3 text-white">Inicia Sessió</span>
                  <script>startApp();</script>
                </li>
                <li><a href="http://<?=$ip_maquina?>:8000/afegir_oferta"><span class="rounded bg-primary py-2 px-3 text-white">
                      <span class="h5 mr-2">+</span> Publica Oferta de Feina</span></a></li>
              </ul>
            </nav>
          </div>

          <div class="col-6 col-xl-2 text-right d-block">

            <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#"
                class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

          </div>

        </div>
      </div>

    </header>

    <div class="site-blocks-cover" style="background-image: url(images/back2.gif);" data-aos="fade"
      data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row row-custom align-items-center">
          <div class="col-md-10">
            <h1 class="mb-2 text-white w-75"><span class="font-weight-bold">Borsa de treball</span> JobIAM.cat</h1>
            <div class="job-search">
              <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active py-3" id="pills-job-tab" data-toggle="pill" href="#pills-job" role="tab"
                    aria-controls="pills-job" aria-selected="true">Troba Feina</a>
                </li>
              </ul>
              <div class="tab-content bg-white p-4 rounded" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-job" role="tabpanel" aria-labelledby="pills-job-tab">
                  <form action="/" method="post">
                    <div class="row">
                      <div class="col-md-6 col-lg-9 mb-3 mb-lg-0">
                        <div class="select-wrap">
                          <span class="icon-keyboard_arrow_down arrow-down"></span>
                          <select name="categoria" class="form-control">
                            <option value="totes">Categories</option>
                            <option id="DAW" value="DAW"></option>
                            <option id="DAM" value="DAM"></option>
                            <option id="ASIX" value="ASIX"></option>
                            <option id="SMIX" value="SMIX"></option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6 col-lg-3 mb-3 mb-lg-0">
                        <input type="submit" class="btn btn-primary btn-block" onclick="filterSelection('nature')" value="Cerca">
                      </div>
                    </div>
                  </form>

                  <?php
                    
                    $categoria_oferta = $_POST["categoria"];

                    // El primer cop que s'obra la paguina ens dona totes les ofertes //
                    if($categoria_oferta==""){
                      $categoria_oferta = "totes";
                    }

                  ?>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section bg-light">
      <div class="container">
        <div class="row justify-content-start text-left mb-5">
          <div class="col-md-9" data-aos="fade">
            <h2 class="font-weight-bold text-black">Ofertes Recents</h2>
          </div>
          <div id="pujaCurr" class="col-md-3" data-aos="fade">
            <label class="btn rounded bg-primary py-2 px-3 text-white" data-toggle="modal" data-target=".currModal">Puja Curriculum</label>
          </div>
            <div class="modal fade currModal" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Pujar Curriculum</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                  <input type="file" id="currFile">
                  <label for="accept"><input type="checkbox" onclick="acceptar()" id="accept"> Accepto enviar el curriculum a la empresa</label>
                  <p id="alertCurr" style="display:none; color:coral"> Has d'acceptar l'enviament! i afegir el curriculum</p>
                  <script>
                    function acceptar() {
                      let curr = document.getElementById("currFile");
                      let checkBox = document.getElementById("accept");
                      let text = document.getElementById("alertCurr");
                      let envia = document.getElementById("modalEnvia");
                      if (checkBox.checked == false || curr.files.length == 0){
                        text.style.display = "block";
                        envia.style.display = "none";
                      }
                      else{
                        text.style.display = "none";
                        envia.style.display = "block";
                      }
                    }
                  </script>
                  </div>
                  <div class="modal-footer">
                    <button type="button" id="modalEnvia" style="display:none;" class="btn btn-default" data-dismiss="modal">Envia</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row" data-aos="fade">
          <div id = "oferta" class="col-md-12"></div>
          </div>
        </div>
      </div>
    </div>
                    
        <!-- OFERTES -->
        <?php
          require 'control_ofertes.php';
        ?>
        <!-- OFERTES -->
       
  </div>
  <footer class="site-footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div class="row">
            <div class="col-6 col-md-3 col-lg-4 mb-5 mb-lg-0">
              <h3 class="footer-heading mb-4">Per Estudiants</h3>
              <ul class="list-unstyled">
                <li><a href="#">Busca Feina</a></li>
              </ul>
            </div>
            <div class="col-6 col-md-3 col-lg-4 mb-5 mb-lg-0">
              <h3 class="footer-heading mb-4">Per Empreses</h3>
              <ul class="list-unstyled">
                <li><a href="http://<?=$ip_maquina?>:8000/afegir_oferta">Publica Oferta</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <h3 class="footer-heading mb-4">Contacte</h3>
          <ul class="list-unstyled">
            <li>
              <span class="d-block text-white">Ubicació</span>
              Av. d'Esplugues, 38, 08034 Barcelona
            </li>
            <li>
              <span class="d-block text-white">Telèfon</span>
              932 033 332
            </li>
            <li>
              <span class="d-block text-white">Adreça electrònica</span>
              admin@iam.cat
            </li>
          </ul>

        </div>
      </div>
      <div class="row mt-5 text-center">
        <div class="col-md-12">
          <p>
            Copyright &copy;
            <script>document.write(new Date().getFullYear());</script> All Rights Reserved | This template is made by <a
              href="https://colorlib.com" target="_blank">Colorlib</a>
          </p>
        </div>

      </div>
    </div>
  </footer>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/main.js"></script>

</body>

</html>

<script>
    $(document).ready(function(){

      // S'agafa totes les cookies per poder agafar la desitjada
      var allcookies = document.cookie;
      cookiearray = allcookies.split(';');
      for(var i=0; i<cookiearray.length; i++) {
        name = cookiearray[i].split('=')[0];
        value = cookiearray[i].split('=')[1];
      }
      correu_user = cookiearray[1];
      if (typeof correu_user !== 'undefined'){
        correu_user = correu_user.split('=');
        correu_user = correu_user[1];
      }

      if (typeof correu_user != 'null'){
        $("#pujaCurr").show();
      }
      else{
        $("#pujaCurr").hide();

      }
    });
    </script>